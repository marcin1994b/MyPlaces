MyPlaces is NOT available on Google Play Store and probably never will.

The app was developed as first quest to take part in 5-days Android Bootcamp.
It's simple application to collect and manage places where you were. 
You can write a simple note, add photo and that's all. You can see your all added places in list view or map view.
To developed it, I used kotlin, sugar ORM, google play services and mvp pattern.