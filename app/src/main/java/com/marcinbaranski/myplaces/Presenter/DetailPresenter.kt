package com.marcinbaranski.myplaces.Presenter

import com.marcinbaranski.myplaces.Model.Entity.Place
import com.marcinbaranski.myplaces.Model.Entity.PlacesAdapterHolder
import com.marcinbaranski.myplaces.Model.MyStrings
import com.marcinbaranski.myplaces.Model.SugarORMHandler

class DetailPresenter(val view: PresenterContracts.PublishToDetailActivity) :
        PresenterContracts.DetailPresenter, SugarORMHandler.onSugarORMHandlerResult {



    override fun loadData(itemPosition: Int) {
        if (itemPosition >= 0){
            PlacesAdapterHolder.placesAdapter?.let {
                val place = it.getItem(itemPosition)
                view.setViews(place.city, place.title, place.note,
                        MyStrings.BASIC_IMG_URL_BIG + place.imgId.toString())
            }
        }
    }

    override fun onEditButtonTap(itemPosition: Int) {
        view.startEditActivity(itemPosition)
    }

    override fun onDeleteButtonTap(itemPosition: Int) {
        PlacesAdapterHolder.placesAdapter?.let{
            val place = it.getItem(itemPosition)
            removeItemFromAdapter(place)
            removePlaceFromDB(place)
        }

    }

    override fun onDBHandlerResult(arr: ArrayList<Place>?) {
        view.closeActivity()
    }

    private fun removeItemFromAdapter(place: Place){
        PlacesAdapterHolder.placesAdapter?.remove(place)
    }

    private fun removePlaceFromDB(place: Place){
        val sugarOrm = SugarORMHandler(MyStrings.DB_ACTION_DELETE, this)
        sugarOrm.execute(place)
    }

}