package com.marcinbaranski.myplaces.Presenter

import com.marcinbaranski.myplaces.Model.Entity.Place

interface PresenterContracts {

    interface PublishToMainActivity {

        fun showToast(message: String)

        fun startDetailActivity(placeId: Int)

        fun refreshListView()

        fun startMapActivity()

    }

    interface PublishToAddNoteActivity {

        fun showToast(message: String)

        fun closeActivity()

        fun setLocationTextView(location: String)

    }

    interface PublishToEditNoteActivity {

        fun showToast(message: String)

        fun closeActivity()

        fun setViews(imgPath: String, location: String, title: String, note: String)
    }

    interface PublishToDetailActivity {

        fun showToast(message: String)

        fun setViews(location: String, title: String, note: String, imgPath: String)

        fun closeActivity()

        fun startEditActivity(itemPosition: Int)
    }

    interface PublishToMapActivity{

        fun showToast(message: String)

        fun addMarkers(list: ArrayList<Place>)
    }

    interface AddNotePresenter {

        fun onStart()

        fun onResume()

        fun onPause()

        fun onStop()

        fun getLastLocation()

        fun onSubmitButtonClick(location: String, title: String, note: String, imgId: String)

        fun onRefreshMenuButtonTap()

        fun onImageViewClick()

    }

    interface EditNotePresenter {

        fun onSubmitButtonClick(location: String, title: String, note: String, imgId: String)

        fun onImageViewClick()

        fun loadData(itemPosition: Int)
    }

    interface DetailPresenter{

        fun loadData(itemPosition: Int)

        fun onEditButtonTap(itemPosition: Int)

        fun onDeleteButtonTap(itemPosition: Int)

    }

    interface MainActivityPresenter{

        fun onListViewTap(position: Int)

        fun loadData()

        fun onMapButtonTap()

    }

    interface MapActivityPresenter{

        fun loadData()

    }

}