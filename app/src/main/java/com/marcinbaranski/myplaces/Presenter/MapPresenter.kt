package com.marcinbaranski.myplaces.Presenter

import com.marcinbaranski.myplaces.Model.Entity.PlacesAdapterHolder

class MapPresenter(val view: PresenterContracts.PublishToMapActivity):
        PresenterContracts.MapActivityPresenter {

    override fun loadData() {
        PlacesAdapterHolder.placesAdapter?.let{
            view.addMarkers(it.list)
        }
    }
}