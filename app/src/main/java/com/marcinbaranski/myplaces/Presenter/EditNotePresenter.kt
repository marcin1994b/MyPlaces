package com.marcinbaranski.myplaces.Presenter

import com.marcinbaranski.myplaces.Model.Entity.Place
import com.marcinbaranski.myplaces.Model.Entity.PlacesAdapterHolder
import com.marcinbaranski.myplaces.Model.MyStrings
import com.marcinbaranski.myplaces.Model.SugarORMHandler

class EditNotePresenter(val view: PresenterContracts.PublishToEditNoteActivity) :
        PresenterContracts.EditNotePresenter, SugarORMHandler.onSugarORMHandlerResult {


    var place: Place = Place(0.0, 0.0, "", "", "", 0)

    var position : Int = -1

    override fun loadData(itemPosition: Int) {

        if (itemPosition >= 0){
            PlacesAdapterHolder.placesAdapter?.let {
                place = it.getItem(itemPosition)
                view.setViews(MyStrings.BASIC_IMG_URL_MED + place.imgId.toString(), place.city,
                        place.title, place.note)
                position = itemPosition
            }
        }
    }

    override fun onSubmitButtonClick(location: String, title: String, note: String, imgId: String) {
        val error: StringBuilder = StringBuilder()

        if(location.isEmpty()){
            error.append(MyStrings.ERR_LOCATION_IS_EMPTY)
        }
        if(title.isEmpty()){
            error.append(MyStrings.ERR_TITLE_IS_EMPTY)
        }
        if(note.isEmpty()){
            error.append(MyStrings.ERR_NOTE_IS_EMPTY)
        }

        if(error.isEmpty()){
            place.title = title
            place.note = note
            //place.imgId = getRandomImgId()
            updatePlaceInAdapter(place)
            updatePlaceInDB(place)

        }else{
            view.showToast(error.toString())
        }
    }

    override fun onDBHandlerResult(arr: ArrayList<Place>?) {
        view.closeActivity()
    }

    override fun onImageViewClick() {
        view.showToast(MyStrings.MSG_FUNCTIONALITY_NOT_AVAILABLE)
    }

    private fun updatePlaceInAdapter(place: Place){
        PlacesAdapterHolder.placesAdapter?.setItem(position, place)
        PlacesAdapterHolder.wasItemUpdated = true
    }

    private fun updatePlaceInDB(place: Place){
        val sugarOrmHandler = SugarORMHandler(MyStrings.DB_ACTION_UPDATE, this)
        sugarOrmHandler.execute(place)
    }


}