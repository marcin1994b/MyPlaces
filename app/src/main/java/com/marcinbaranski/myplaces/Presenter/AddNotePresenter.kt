package com.marcinbaranski.myplaces.Presenter

import android.content.Context
import com.marcinbaranski.myplaces.Model.Entity.Place
import com.marcinbaranski.myplaces.Model.Entity.PlacesAdapterHolder
import com.marcinbaranski.myplaces.Model.LocationAPIHandler
import com.marcinbaranski.myplaces.Model.MyStrings
import com.marcinbaranski.myplaces.Model.SugarORMHandler
import java.util.*

class AddNotePresenter(val context: Context, val view: PresenterContracts.PublishToAddNoteActivity) :
        PresenterContracts.AddNotePresenter, LocationAPIHandler.OnLocationApiHandlerResult,
        SugarORMHandler.onSugarORMHandlerResult {

    val MAX_IMG_ID = 100

    val locationApiHandler : LocationAPIHandler = LocationAPIHandler(context, this)
    val place : Place = Place(0.0, 0.0, "", "", "", 0)
    val seed: Random = Random()

    var isLocationSet : Boolean = false

    override fun onLocationApiHandlerResult(isSuccessful: Boolean, message: String,
                                            data: LocationAPIHandler.LocationHandlerResponse?) {
        when(isSuccessful){
            true -> {
                data?.let{
                    place.longitude = data.longitude
                    place.latitude = data.latitude
                    place.city = data.addressLine
                    view.setLocationTextView(data.addressLine)
                    view.showToast(MyStrings.MSG_LAST_KNOWN_LOCATION_SET)
                    isLocationSet = true
                }
            }
            false -> view.showToast(message)
        }
    }

    override fun onStart() {
        locationApiHandler.onStart()
    }

    override fun onResume() {
        locationApiHandler.onResume()
    }

    override fun onPause() {
        locationApiHandler.onPause()
    }

    override fun onStop() {
        locationApiHandler.onStop()
    }

    override fun getLastLocation() {
        locationApiHandler.getLocation()
    }

    override fun onSubmitButtonClick(location: String, title: String, note: String, imgId: String) {
        val error: StringBuilder = StringBuilder()

        if(location.isEmpty()){
            error.append(MyStrings.ERR_LOCATION_IS_EMPTY)
        }
        if(title.isEmpty()){
            error.append(MyStrings.ERR_TITLE_IS_EMPTY)
        }
        if(note.isEmpty()){
            error.append(MyStrings.ERR_NOTE_IS_EMPTY)
        }
        if(!isLocationSet){
            error.append(MyStrings.ERR_LOCATION_NOT_SET)
        }

        if(error.isEmpty()){
            place.title = title
            place.note = note
            place.imgId = getRandomImgId()
            val sugarOrmHandler = SugarORMHandler(MyStrings.DB_ACTION_SAVE, this)
            sugarOrmHandler.execute(place)
            addPlaceToPlacesAdapter(place)
        }else{
            view.showToast(error.toString())
        }
    }

    override fun onRefreshMenuButtonTap() {
        getLastLocation()
    }

    override fun onImageViewClick(){
        view.showToast(MyStrings.MSG_FUNCTIONALITY_NOT_AVAILABLE)
    }

    override fun onDBHandlerResult(arr: ArrayList<Place>?) {
        view.showToast(MyStrings.MSG_PLACES_SUCCESSFUL_ADDED)
        view.closeActivity()
    }

    private fun addPlaceToPlacesAdapter(place: Place){
        PlacesAdapterHolder.placesAdapter?.add(place)
    }

    private fun getRandomImgId() : Int{
        return seed.nextInt(MAX_IMG_ID)
    }
}