package com.marcinbaranski.myplaces.Presenter

import com.marcinbaranski.myplaces.Model.Entity.Place
import com.marcinbaranski.myplaces.Model.Entity.PlacesAdapterHolder
import com.marcinbaranski.myplaces.Model.MyStrings
import com.marcinbaranski.myplaces.Model.SugarORMHandler

class MainActivityPresenter(val view: PresenterContracts.PublishToMainActivity) :
        PresenterContracts.MainActivityPresenter, SugarORMHandler.onSugarORMHandlerResult {


    override fun onDBHandlerResult(arr: ArrayList<Place>?) {
        arr?.let{
            PlacesAdapterHolder.placesAdapter?.addAll(arr)
            view.refreshListView()
            view.showToast(MyStrings.MSG_DATA_LOADED)
        }
    }

    override fun onListViewTap(position: Int) {
        view.startDetailActivity(position)
    }

    override fun loadData() {
        if(isPlacesAdapterEmpty()){
            view.showToast(MyStrings.MSG_LOADING_DATA)
            val sugarOrmHandler = SugarORMHandler(MyStrings.DB_ACTION_GET, this)
            sugarOrmHandler.execute(null)
        }else{
            if(PlacesAdapterHolder.wasItemUpdated){
                PlacesAdapterHolder.placesAdapter?.clear()
                PlacesAdapterHolder.wasItemUpdated = false
                loadData()
            }else{
                view.refreshListView()
            }
        }
    }

    override fun onMapButtonTap() {
        view.startMapActivity()
    }

    private fun isPlacesAdapterEmpty() : Boolean{
        return PlacesAdapterHolder.placesAdapter?.count == 0
    }

}