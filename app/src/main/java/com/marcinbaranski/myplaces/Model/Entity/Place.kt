package com.marcinbaranski.myplaces.Model.Entity

import com.orm.SugarRecord

open class Place() : SugarRecord<Place>() {

    var latitude: Double = 0.0
    var longitude: Double = 0.0
    var city: String = ""
    var title: String = ""
    var note: String = ""
    var imgId: Int = -1

    constructor(latitude: Double,
                longitude: Double,
                city: String,
                title: String,
                note: String,
                imgId: Int) : this() {
        this.latitude = latitude
        this.longitude = longitude
        this.city = city
        this.title = title
        this.note = note
        this.imgId = imgId
    }

}