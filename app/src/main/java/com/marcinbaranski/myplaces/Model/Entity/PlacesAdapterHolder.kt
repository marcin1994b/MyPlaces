package com.marcinbaranski.myplaces.Model.Entity

import com.marcinbaranski.myplaces.Model.PlacesAdapter

object PlacesAdapterHolder {

    var placesAdapter : PlacesAdapter? = null
    var wasItemUpdated : Boolean = false

}