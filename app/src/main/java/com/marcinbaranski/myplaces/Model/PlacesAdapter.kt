package com.marcinbaranski.myplaces.Model

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import com.marcinbaranski.myplaces.Model.Entity.Place
import com.marcinbaranski.myplaces.R
import com.squareup.picasso.Picasso

class PlacesAdapter(context: Context, resource: Int, var list: ArrayList<Place>) :
        ArrayAdapter<Place>(context, resource, list){



    override fun getItemId(position: Int): Long {
        return list[position].id.toLong()
    }

    fun setItem(position: Int, place: Place){
        if(position >= 0 && position < list.size){
            list[position] = place
        }
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View? {
        var view = convertView
        if(view == null){
            val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            view = inflater.inflate(R.layout.place_item, null)
        }

        view?.let{
            val item = list[position]
            setTitle(item.title, it)
            setLocation(item.city, it)
            setNote(item.note, it)
            setImg(item.imgId, it)
        }

        return view
    }

    override fun getCount(): Int {
        return list.size
    }

    private fun setTitle(title: String, v: View){
        v.findViewById<TextView>(R.id.title).text = title
    }

    private fun setLocation(location: String, v: View){
        v.findViewById<TextView>(R.id.place).text = location
    }

    private fun setNote(note: String, v: View){
        v.findViewById<TextView>(R.id.note).text = note
    }

    private fun setImg(imgId: Int, v: View){
        val str: StringBuilder = StringBuilder()
        str.append(MyStrings.BASIC_IMG_URL_SMALL).append(imgId.toString())
        Picasso.with(context)
                .load(str.toString())
                .placeholder(R.drawable.imgplaceholder)
                .error(R.drawable.noimageblue)
                .fit()
                .into(v.findViewById<ImageView>(R.id.image))
    }

}