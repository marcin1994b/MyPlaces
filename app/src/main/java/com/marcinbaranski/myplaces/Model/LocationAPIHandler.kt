package com.marcinbaranski.myplaces.Model

import android.content.Context
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.net.ConnectivityManager
import android.os.Bundle
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationListener
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import java.util.*



class LocationAPIHandler(val context: Context, val presenter: OnLocationApiHandlerResult) :
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    data class LocationHandlerResponse(var longitude: Double,
                                       var latitude: Double,
                                       var addressLine: String)

    interface OnLocationApiHandlerResult{

        fun onLocationApiHandlerResult(isSuccessful: Boolean, message: String,
                                       data: LocationHandlerResponse?)
    }

    private var mLastLocation: Location? = null

    private lateinit var mGoogleAPIClient: GoogleApiClient
    private lateinit var mLocationRequest : LocationRequest

    private var mRequestLocationUpdates: Boolean = false

    private val UPDATE_INTERVAL : Long = 10000
    private val FASTEST_INTERVAL : Long = 5000
    private val DISPLACEMENT : Float = 10.0F

    init {
        if(checkPlayServices()){
            buildGoogleApiClient()
            createLocationRequest()
        }
    }

    fun onStart(){
        mGoogleAPIClient.connect()
    }

    fun onResume(){
        checkPlayServices()
        if(mGoogleAPIClient.isConnected && mRequestLocationUpdates){
            startLocationUpdates()
        }
    }

    fun onStop() {
        if(mGoogleAPIClient.isConnected){
            mGoogleAPIClient.disconnect()
        }
    }

    fun onPause(){
        stopLocationUpdates()
    }


    fun getLocation(){
        if(isNetworkAvailable()) {
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleAPIClient)
            mLastLocation?.let {
                val address: String = getLocationName(it.longitude, it.latitude)
                if (address.isNotEmpty()) {
                    val data = LocationHandlerResponse(it.longitude, it.latitude, address)
                    presenter.onLocationApiHandlerResult(true, "", data)
                } else {
                    presenter.onLocationApiHandlerResult(false, MyStrings.ERR_COULDNT_GET_ADDRESS, null)
                }
            } ?: presenter.onLocationApiHandlerResult(false, MyStrings.ERR_COULDNT_GET_LOCATION, null)
        }else{
            presenter.onLocationApiHandlerResult(false, MyStrings.ERR_NO_INTERNET, null)
        }
    }

    private fun getLocationName(longitude: Double, latitude: Double) : String{
        println(longitude)
        println(latitude)
        val geocoder: Geocoder = Geocoder(context, Locale.getDefault())
        val addressList : List<Address>? = geocoder.getFromLocation(latitude, longitude, 1)
        addressList?.let{
            if(addressList.isNotEmpty()){
                return addressList[0].getAddressLine(0)
            }
        }
        return ""
    }

    fun buildGoogleApiClient() {
        mGoogleAPIClient = GoogleApiClient.Builder(context)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build()
    }

    fun createLocationRequest(){
        mLocationRequest = LocationRequest()
        mLocationRequest.interval = UPDATE_INTERVAL
        mLocationRequest.fastestInterval = FASTEST_INTERVAL
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest.smallestDisplacement = DISPLACEMENT
    }

    fun checkPlayServices() : Boolean {
        val googleAPI = GoogleApiAvailability.getInstance()
        val resultCode = googleAPI.isGooglePlayServicesAvailable(context)
        if(resultCode != ConnectionResult.SUCCESS){
            if(googleAPI.isUserResolvableError(resultCode)){
                presenter.onLocationApiHandlerResult(false, resultCode.toString(), null)
            }else{
                presenter.onLocationApiHandlerResult(false, MyStrings.ERR_DEVICE_NOT_SUPPORTED, null)
            }
            return false
        }
        return true
    }

    fun startLocationUpdates(){
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleAPIClient, mLocationRequest, this)
    }

    fun stopLocationUpdates(){
        if(mGoogleAPIClient.isConnected) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleAPIClient, this)
        }
    }

    override fun onConnected(bundle: Bundle?) {
        getLocation()
        if(mRequestLocationUpdates){
            startLocationUpdates()
        }
    }

    override fun onConnectionSuspended(p0: Int) {
        mGoogleAPIClient.connect()
    }

    override fun onConnectionFailed(p0: ConnectionResult) {
        presenter.onLocationApiHandlerResult(false, MyStrings.ERR_CONNECTION_FAILED, null)
    }

    override fun onLocationChanged(location: Location?) {
       location?.let{
           mLastLocation = it
       }
    }

    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }


}