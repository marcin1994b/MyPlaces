package com.marcinbaranski.myplaces.Model

import android.os.AsyncTask
import com.marcinbaranski.myplaces.Model.Entity.Place
import com.orm.query.Select

class SugarORMHandler(val action: Int, var presenter: onSugarORMHandlerResult):
        AsyncTask<Place, Void, ArrayList<Place>?>() {

    interface onSugarORMHandlerResult{
        fun onDBHandlerResult(arr: ArrayList<Place>?)
    }



    override fun doInBackground(vararg place: Place?): ArrayList<Place>? {
        when(action) {
            MyStrings.DB_ACTION_SAVE-> return save(place[0])
            MyStrings.DB_ACTION_GET -> return getAll()
            MyStrings.DB_ACTION_UPDATE -> return update(place[0])
            MyStrings.DB_ACTION_DELETE -> return delete(place[0])
            }
        return null
    }

    override fun onPostExecute(result: ArrayList<Place>?) {
        super.onPostExecute(result)
        presenter.onDBHandlerResult(result)
    }


    private fun getAll() : ArrayList<Place>?{
        return ArrayList<Place>(Select.from(Place::class.java).list())
    }

    private fun save(place: Place?) : ArrayList<Place>?{
        place?.let{
            place.save()
        }
        return null
    }

    private fun update(place: Place?) : ArrayList<Place>?{
        return save(place)
    }

    private fun delete(place: Place?) : ArrayList<Place>? {
        place?.let{
            place.delete()
        }
        return null
    }

}