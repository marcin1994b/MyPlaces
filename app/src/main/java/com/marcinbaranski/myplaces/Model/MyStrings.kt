package com.marcinbaranski.myplaces.Model

object MyStrings {

    val MSG_FUNCTIONALITY_NOT_AVAILABLE = "This functionality is not available yet"
    val MSG_PLACES_SUCCESSFUL_ADDED = "Your place was successful added."
    val MSG_LAST_KNOWN_LOCATION_SET = "The last known location was set"
    val MSG_DATA_LOADED = "Data loaded"
    val MSG_LOADING_DATA = "Loading data..."

    val ERR_PERMISSION_DENIED = "We need Location Permission to get you current location"
    val ERR_COULDNT_GET_LOCATION = "Couldn't get the location. Make sure location is enabled on the device"
    val ERR_COULDNT_GET_ADDRESS = "Couldn't get the location address."
    val ERR_NO_INTERNET = "No Internet connection available."
    val ERR_CONNECTION_FAILED = "Connection failed"
    val ERR_DEVICE_NOT_SUPPORTED = "This device is not supported"
    val ERR_LOCATION_NOT_SET = "Location is not set. "
    val ERR_LOCATION_IS_EMPTY = "Location is empty. "
    val ERR_TITLE_IS_EMPTY = "Title is empty. "
    val ERR_NOTE_IS_EMPTY = "Note is empty. "

    val DB_ACTION_SAVE = 0
    val DB_ACTION_GET = 1
    val DB_ACTION_UPDATE = 2
    val DB_ACTION_DELETE = 3

    val INTENT_KEY_ITEM_ID = "itemPosition"

    val BASIC_IMG_URL_BIG = "https://unsplash.it/300/300?image="
    val BASIC_IMG_URL_MED = "https://unsplash.it/200/200?image="
    val BASIC_IMG_URL_SMALL = "https://unsplash.it/150/150?image="
}