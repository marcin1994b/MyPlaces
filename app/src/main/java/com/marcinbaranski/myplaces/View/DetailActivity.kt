package com.marcinbaranski.myplaces.View

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.marcinbaranski.myplaces.Model.MyStrings
import com.marcinbaranski.myplaces.Presenter.DetailPresenter
import com.marcinbaranski.myplaces.Presenter.PresenterContracts
import com.marcinbaranski.myplaces.R
import com.squareup.picasso.Picasso

class DetailActivity : AppCompatActivity(), PresenterContracts.PublishToDetailActivity{

    lateinit var presenter : PresenterContracts.DetailPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        initPresenter()
        initToolbar()
    }

    override fun onResume() {
        super.onResume()
        presenter.loadData(intent.getIntExtra(MyStrings.INTENT_KEY_ITEM_ID, -1))
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.detail_activity_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.delete -> {
                presenter.onDeleteButtonTap(intent.getIntExtra(MyStrings.INTENT_KEY_ITEM_ID, -1))
            }
            R.id.edit -> {
                presenter.onEditButtonTap(intent.getIntExtra(MyStrings.INTENT_KEY_ITEM_ID, -1))
            }
        }
        return false
    }

    override fun closeActivity() {
        finish()
    }

    override fun startEditActivity(itemPosition: Int) {
        val intent = Intent(this, EditNoteActivity::class.java)
        intent.putExtra(MyStrings.INTENT_KEY_ITEM_ID, itemPosition)
        startActivity(intent)
    }

    override fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun setViews(location: String, title: String, note: String, imgPath: String) {
        setLocationTextView(location)
        setTitleTextView(title)
        setNoteTextView(note)
        setImgView(imgPath)
    }

    private fun initPresenter(){
        presenter = DetailPresenter(this)
    }
    private fun initToolbar(){
        val toolbar = findViewById(R.id.my_toolbar) as Toolbar
        toolbar.title = getString(R.string.title_activity_detail)
        setSupportActionBar(toolbar)
        supportActionBar?.let{
            it.setDisplayHomeAsUpEnabled(true)
            it.setDisplayShowHomeEnabled(true)
        }

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }


    private fun setLocationTextView(location: String){
        (this.findViewById(R.id.place_textview) as TextView).text = location
    }

    private fun setTitleTextView(title: String){
        (this.findViewById(R.id.title_textview) as TextView).text = title
    }

    private fun setNoteTextView(note: String){
        (this.findViewById(R.id.note_textview) as TextView).text = note
    }

    private fun setImgView(imgPath: String){
        Picasso.with(this)
                .load(imgPath)
                .placeholder(R.drawable.imgplaceholder)
                .error(getDrawable(R.drawable.noimageblue))
                .fit()
                .into((this.findViewById(R.id.image) as ImageView))
    }
}
