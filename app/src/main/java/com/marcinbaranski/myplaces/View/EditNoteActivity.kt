package com.marcinbaranski.myplaces.View

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.widget.*
import com.marcinbaranski.myplaces.Model.MyStrings
import com.marcinbaranski.myplaces.Presenter.EditNotePresenter
import com.marcinbaranski.myplaces.Presenter.PresenterContracts
import com.marcinbaranski.myplaces.R
import com.squareup.picasso.Picasso

class EditNoteActivity : AppCompatActivity(), PresenterContracts.PublishToEditNoteActivity {

    lateinit var presenter : PresenterContracts.EditNotePresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_note)
        initPresenter()
        initToolbar()
        initSubmitButton()
        initImg()
        presenter.loadData(intent.getIntExtra(MyStrings.INTENT_KEY_ITEM_ID, -1))
    }


    override fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun closeActivity() {
        finish()
    }

    override fun setViews(imgPath: String, location: String, title: String, note: String) {
        setImgView(imgPath)
        setLocationTextView(location)
        setTitleEditText(title)
        setNoteEditText(note)
    }

    private fun setImgView(imgPath: String){
        Picasso.with(this)
                .load(imgPath)
                .placeholder(R.drawable.imgplaceholder)
                .error(getDrawable(R.drawable.noimageblue))
                .fit()
                .into((this.findViewById(R.id.image) as ImageView))

    }

    private fun getImgPath(): String{
        return ""
    }

    private fun setLocationTextView(location: String){
        (findViewById(R.id.location_textview) as TextView).text = location
    }

    private fun getLocationFromTextView(): String{
        return (findViewById(R.id.location_textview) as TextView).text.toString()
    }

    private fun setTitleEditText(title: String){
        (findViewById(R.id.title_edittext) as EditText).setText(title, TextView.BufferType.EDITABLE)
    }

    private fun getTitleFromEditText(): String{
        return (findViewById(R.id.title_edittext) as EditText).text.toString()
    }

    private fun setNoteEditText(note: String){
        (findViewById(R.id.note_edittext) as EditText).setText(note, TextView.BufferType.EDITABLE)
    }

    private fun getNoteFromEditText(): String{
        return (findViewById(R.id.note_edittext) as EditText).text.toString()
    }

    private fun initPresenter(){
        presenter = EditNotePresenter(this)
    }

    private fun initToolbar(){
        val toolbar = findViewById(R.id.my_toolbar) as Toolbar
        toolbar.title = getString(R.string.title_activity_edit)
        setSupportActionBar(toolbar)
        supportActionBar?.let{
            it.setDisplayHomeAsUpEnabled(true)
            it.setDisplayShowHomeEnabled(true)
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun initSubmitButton(){
        (findViewById(R.id.submit_button) as Button).setOnClickListener {
            presenter.onSubmitButtonClick(getLocationFromTextView(),
                    getTitleFromEditText(), getNoteFromEditText(), getImgPath())
        }
    }

    private fun initImg(){
        (findViewById(R.id.image) as ImageView).background = getDrawable(R.drawable.noimageblue)
        (findViewById(R.id.image) as ImageView).setOnClickListener {
            presenter.onImageViewClick()
        }
    }



}
