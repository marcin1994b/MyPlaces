package com.marcinbaranski.myplaces.View

import android.Manifest
import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import android.widget.*
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import com.marcinbaranski.myplaces.Model.MyStrings
import com.marcinbaranski.myplaces.Presenter.AddNotePresenter
import com.marcinbaranski.myplaces.Presenter.PresenterContracts
import com.marcinbaranski.myplaces.R

class AddNoteActivity : AppCompatActivity(), PresenterContracts.PublishToAddNoteActivity{


    var presenter: PresenterContracts.AddNotePresenter? = null
    lateinit var mContext : Context

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_note)
        mContext = this.baseContext
        setPresenters()
        initToolbar()
        initImg()
        initSubmitButton()
        initDexterPermission()
    }

    override fun onResume() {
        super.onResume()
        presenter?.onResume()
    }

    override fun onPause() {
        super.onPause()
        presenter?.onPause()
    }

    override fun onStop() {
        super.onStop()
        presenter?.onStop()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.add_note_activity_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.refresh -> {
                presenter?.onRefreshMenuButtonTap()
            }
        }
        return false
    }

    private fun initToolbar(){
        val toolbar = findViewById(R.id.my_toolbar) as Toolbar
        toolbar.title = getString(R.string.title_activity_add)
        setSupportActionBar(toolbar)
        supportActionBar?.let{
            it.setDisplayHomeAsUpEnabled(true)
            it.setDisplayShowHomeEnabled(true)
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun initImg(){
        (findViewById(R.id.image) as ImageView).setOnClickListener {
            presenter?.onImageViewClick()
        }
    }

    override fun setLocationTextView(location : String) {
        (this.findViewById(R.id.location_textview) as TextView).text = location
    }

    override fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun closeActivity() {
        this.finish()
    }

    private fun setPresenters(){
        presenter = AddNotePresenter(mContext, this)
    }

    private fun initDexterPermission(){
        Dexter.withActivity(this)
                .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(object : PermissionListener {
                    override fun onPermissionGranted(response: PermissionGrantedResponse) {
                        presenter?.onStart()
                    }

                    override fun onPermissionDenied(response: PermissionDeniedResponse) {
                        Toast.makeText(mContext, MyStrings.ERR_PERMISSION_DENIED, Toast.LENGTH_SHORT).show()
                    }

                    override fun onPermissionRationaleShouldBeShown(permission: PermissionRequest,
                                                                    token: PermissionToken) {
                        token.continuePermissionRequest()
                    }
                }).check()
    }

    private fun getLocationFromTextView() : String {
        return (this.findViewById(R.id.location_textview) as TextView).text.toString()
    }

    private fun getTitleFromEditText() : String{
        return (this.findViewById(R.id.title_edittext) as EditText).text.toString()
    }

    private fun getNoteFromEditText() : String{
        return (this.findViewById(R.id.note_edittext) as EditText).text.toString()
    }

    private fun getImgPath() : String {
        return ""
    }

    private fun initSubmitButton(){
        (this.findViewById(R.id.submit_button) as Button).setOnClickListener {
            presenter?.onSubmitButtonClick(getLocationFromTextView(),
                    getTitleFromEditText(),
                    getNoteFromEditText(),
                    getImgPath())
        }
    }
}
