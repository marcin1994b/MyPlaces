package com.marcinbaranski.myplaces.View

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.widget.Toast
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.marcinbaranski.myplaces.Model.Entity.Place
import com.marcinbaranski.myplaces.Presenter.MapPresenter
import com.marcinbaranski.myplaces.Presenter.PresenterContracts
import com.marcinbaranski.myplaces.R

class MapActivity : AppCompatActivity(), OnMapReadyCallback, PresenterContracts.PublishToMapActivity {

    private var mMap: GoogleMap? = null

    private var presenter : PresenterContracts.MapActivityPresenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_map)
        initPresenter()
        initToolbar()
        initMap()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        presenter?.loadData()
    }

    override fun addMarkers(list: ArrayList<Place>){
        mMap?.let{
            for (item in list) {
                it.addMarker(MarkerOptions().position(LatLng(item.latitude, item.longitude))
                        .title(item.title))
            }
        }
    }

    override fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    fun initPresenter(){
        presenter = MapPresenter(this)
    }

    private fun initMap(){
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    private fun initToolbar(){
        val toolbar = findViewById(R.id.my_toolbar) as Toolbar
        toolbar.title = getString(R.string.title_activity_maps)
        setSupportActionBar(toolbar)
        supportActionBar?.let{
            it.setDisplayHomeAsUpEnabled(true)
            it.setDisplayShowHomeEnabled(true)
        }
    }
}
