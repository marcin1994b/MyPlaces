package com.marcinbaranski.myplaces.View

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import android.widget.ListView
import android.widget.Toast
import com.marcinbaranski.myplaces.Model.Entity.Place
import com.marcinbaranski.myplaces.Model.Entity.PlacesAdapterHolder
import com.marcinbaranski.myplaces.Model.MyStrings
import com.marcinbaranski.myplaces.Model.PlacesAdapter
import com.marcinbaranski.myplaces.Presenter.MainActivityPresenter
import com.marcinbaranski.myplaces.Presenter.PresenterContracts
import com.marcinbaranski.myplaces.R


class MainActivity : AppCompatActivity(), PresenterContracts.PublishToMainActivity{

    lateinit var mContext : Context
    lateinit var presenter : PresenterContracts.MainActivityPresenter

    var listView : ListView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mContext = this.baseContext
        initToolbar()
        initPresenter()
        initFAB()
        initListView()
        initAdapterHolder()
    }

    override fun onResume() {
        super.onResume()
        if(listView != null){
            presenter.loadData()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main_activity_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.map -> {
                presenter.onMapButtonTap()
            }
        }
        return false
    }


    override fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun startDetailActivity(placeId: Int) {
        val intent = Intent(this, DetailActivity::class.java)
        intent.putExtra(MyStrings.INTENT_KEY_ITEM_ID, placeId)
        startActivity(intent)
    }

    override fun startMapActivity() {
        val intent = Intent(this, MapActivity::class.java)
        startActivity(intent)
    }

    override fun refreshListView() {
        listView?.deferNotifyDataSetChanged()
    }

    private fun initListView(){
        listView = findViewById(R.id.list_view) as ListView
        listView?.setOnItemClickListener { _, _, position, _ ->
            presenter.onListViewTap(position)
        }
    }

    private fun initToolbar(){
        val toolbar = findViewById(R.id.my_toolbar) as Toolbar
        toolbar.title = getString(R.string.title_activity_main)
        setSupportActionBar(toolbar)
    }

    private fun initPresenter(){
        presenter = MainActivityPresenter(this)
    }

    private fun initAdapterHolder(){
        PlacesAdapterHolder.placesAdapter = PlacesAdapter(this.baseContext, R.layout.place_item, ArrayList<Place>())
        listView?.adapter = PlacesAdapterHolder.placesAdapter
    }

    private fun initFAB(){
        val fab = findViewById(R.id.fab) as FloatingActionButton
        fab.setOnClickListener({ _ ->
            val intent = Intent(this, AddNoteActivity::class.java)
            startActivity(intent)
        })
    }



}
